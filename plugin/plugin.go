package main

import (
	"fmt"
	"os"
	"strings"
	"unsafe"

	logg "log"

	"gitlab.com/aerth/simpledb"
	"go.etcd.io/bbolt"
)

/*

typedef struct read_response_t {
  size_t length;
  char* data;
  int error; // if error, data is error
} read_response_t;
*/
import "C"

var log = logg.New(os.Stderr, "[simpledb.so] ", logg.Lshortfile)

func main() {
	panic("this is a plugin")
}

var dbs = map[uint]*simpledb.SimpleDB{}

//export db_open
func db_open(id C.uint, s *C.char) *C.char {
	_, ok := dbs[uint(id)]
	if ok {
		return C.CString("exists")
	}
	db, err := simpledb.Load(C.GoString(s))
	if err != nil {
		return C.CString(err.Error())
	}
	dbs[uint(id)] = db
	return nil
}

//export db_close
func db_close(id C.uint) *C.char {
	m, ok := dbs[uint(id)]
	if !ok {
		return C.CString("db key not found")
	}
	err := m.Close()
	if err != nil {
		return C.CString(err.Error())
	}
	return nil

}

//export db_write
func db_write(id C.uint, path *C.char, val *C.char, length C.int) *C.char {
	m, ok := dbs[uint(id)]
	if !ok {
		return C.CString("db key not found")
	}
	paths := strings.Split(strings.TrimSuffix(strings.TrimPrefix(C.GoString(path), "/"), "/"), "/")
	l := len(paths)
	if l < 2 || paths[0] == "" || paths[1] == "" {
		return C.CString("invalid input")
	}
	err := m.DB().Update(func(tx *bbolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(paths[0]))
		if err != nil {
			return fmt.Errorf("inloop* create first bucket err: %v", err)
		}
		for i := 1; i < len(paths); i++ {
			if i == l-1 {
				bucket.Put([]byte(paths[i]), []byte(C.GoBytes(unsafe.Pointer(val), length)))
				continue
			}
			bucket, err = bucket.CreateBucketIfNotExists([]byte(paths[i]))
			if err != nil {
				return fmt.Errorf("inloop createbucket err: %v", err)
			}

		}

		return nil
	})
	if err != nil {
		return C.CString(err.Error())
	}
	return nil
}

//export db_write_string
func db_write_string(id C.uint, path *C.char, val *C.char) *C.char {
	m, ok := dbs[uint(id)]
	if !ok {
		return C.CString("db key not found")
	}
	paths := strings.Split(strings.TrimSuffix(strings.TrimPrefix(C.GoString(path), "/"), "/"), "/")
	l := len(paths)
	if l < 2 || paths[0] == "" || paths[1] == "" {
		return C.CString("invalid input")
	}
	err := m.DB().Update(func(tx *bbolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(paths[0]))
		if err != nil {
			return fmt.Errorf("inloop* create first bucket err: %v", err)
		}
		for i := 1; i < len(paths); i++ {
			if i == l-1 {
				bucket.Put([]byte(paths[i]), []byte(C.GoString(val)))
				continue
			}
			bucket, err = bucket.CreateBucketIfNotExists([]byte(paths[i]))
			if err != nil {
				return fmt.Errorf("inloop createbucket err: %v", err)
			}

		}

		return nil
	})
	if err != nil {
		return C.CString(err.Error())
	}
	return nil
}

func newResponse(data interface{}) C.read_response_t {

	var s string
	var nErr = 0
	switch data.(type) {
	case string:
		s = data.(string)
	case error:
		s = data.(error).Error()
		nErr = 1
	case ([]byte):
		s = string(data.([]byte))
	default:
		panic("nope")
	}
	return C.read_response_t{
		length: C.ulong(len(s)),
		data:   C.CString(s),
		error:  C.int(nErr),
	}

}

//export db_read
func db_read(id C.uint, path *C.char) C.read_response_t {
	m, ok := dbs[uint(id)]
	if !ok {
		s := "db key not found"
		return C.read_response_t{
			length: C.ulong(len(s)),
			data:   C.CString(s),
			error:  1}
	}
	paths := strings.Split(strings.TrimSuffix(strings.TrimPrefix(C.GoString(path), "/"), "/"), "/")
	l := len(paths)
	if l < 2 || paths[0] == "" || paths[1] == "" {
		return newResponse(fmt.Errorf("invalid input"))
	}
	var output []byte
	err := m.DB().View(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket([]byte(paths[0]))
		if bucket == nil {
			return fmt.Errorf("inloop* view first bucket doesnt exist: %q", paths[0])
		}
		for i := 1; i < len(paths); i++ {
			if i == l-1 {
				output = bucket.Get([]byte(paths[i]))
				continue
			}
			bucket = bucket.Bucket([]byte(paths[i]))
			if bucket == nil {
				return fmt.Errorf("inloop createbucket err: nil bucket %q", paths[i])
			}
		}
		return nil
	})
	if err != nil {
		return newResponse(err)
	}

	return newResponse(output)
}
