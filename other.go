package simpledb

import (
	"encoding/json"
	"errors"
	"fmt"

	"go.etcd.io/bbolt"
)

// DB returns the underlying *bbolt.DB if in fact a bbolt DB.
func (s *SimpleDB) DB() *bbolt.DB {
	if s.D == nil {
		return nil
	}
	db, ok := s.D.(*bbolt.DB)
	if !ok {
		println("developer: use only *bbolt.DB")
		return nil
	}
	return db
}

// aliases View/Update/Delete

// View wraps db.View
func (s *SimpleDB) View(txfn func(tx *Tx) error) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.View(txfn)
}

// Update wraps db.Update
func (s *SimpleDB) Update(txfn func(tx *Tx) error) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.Update(txfn)
}

// Unmarshal a nested value
func (s *SimpleDB) UnmarshalNested(bucket, nestedname, key string, thingPtr interface{}) error {
	b := s.GetNested(bucket, nestedname, key)
	if len(b) == 0 {
		return ErrNotFound
	}
	return json.Unmarshal(b, thingPtr)

}

// Unmarshal a value directly into thingPtr
func (s *SimpleDB) Unmarshal(bucket, key string, thingPtr interface{}) error {
	b := s.Get(bucket, key)
	if len(b) == 0 {
		return ErrNotFound
	}
	return json.Unmarshal(b, thingPtr)

}

// GetNested returns the value of the nested bucket at key
func (s *SimpleDB) GetNested(bucket, nestedname, key string) []byte {
	db := s.DB()
	if db == nil {
		return nil
	}
	var b []byte
	db.View(func(t *Tx) error {
		bu := t.Bucket([]byte(bucket))
		if bu == nil {
			return nil
		}
		n := bu.Bucket([]byte(nestedname))
		if n == nil {
			return nil
		}
		b = n.Get([]byte(key))
		return nil
	})
	return b
}

// GetNested returns the value of the nested bucket at key
func (s *SimpleDB) GetPath(path []string) []byte {
	if path == nil {
		return nil
	}
	db := s.DB()
	if db == nil {
		return nil
	}
	var b []byte

	l := len(path) // warning: not trimming space this low
	switch l {
	case 0:
		panic("bad path: 0")
	case 1:
		panic("bad path: 1")
	case 2:
		return s.Get(path[0], path[1])
	case 3:
		return s.GetNested(path[0], path[1], path[2])
	default:
		// this func
	}
	_ = db.View(func(t *Tx) error {
		bu := t.Bucket([]byte(path[0]))
		if bu == nil {
			return nil
		}
		for i := 1; i < l-1; i++ {
			bu = bu.Bucket([]byte(path[i]))
			if bu == nil {
				return nil
			}
		}
		b = bu.Get([]byte(path[l-1]))
		return nil
	})
	return b
}
func (s *SimpleDB) UnmarshalPath(path []string, thingPtr interface{}) error {
	b := s.GetPath(path)
	if b == nil {
		return ErrNotFound
	}
	return json.Unmarshal(b, thingPtr)
}
func (s *SimpleDB) MarshalPath(path []string, thing interface{}) error {
	buf, err := json.Marshal(thing)
	if err != nil {
		return err
	}
	return s.PutPath(path, buf)
}

// PutPath writes to database
func (s *SimpleDB) PutPath(path []string, data []byte) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	l := len(path)
	switch l {
	case 0:
		panic("bad path: 0")
	case 1:
		panic("bad path: 1 (use CreateBucket)")
	case 2:
		return s.Put(path[0], path[1], data)
	case 3:
		return s.PutNested(path[0], path[1], path[2], data)
	default:
		// this func
	}
	// size := len(data)

	return db.Update(func(t *Tx) error {
		bu, err := t.CreateBucketIfNotExists([]byte(path[0]))
		if err != nil {
			return err
		}
		for i := 1; i < l-1; i++ {
			bu, err = bu.CreateBucketIfNotExists([]byte(path[i]))
			if err != nil {
				return fmt.Errorf("creating bucket #%d (%q): %v", i, path[i], err)
			}
			if bu == nil {
				return nil
			}
		}
		return bu.Put([]byte(path[l-1]), data)
	})
}

// Get returns the value of bucket[key]
func (s *SimpleDB) Get(bucket, key string) []byte {
	db := s.DB()
	if db == nil {
		return nil
	}
	var b []byte
	db.View(func(t *Tx) error {
		bu := t.Bucket([]byte(bucket))
		if bu != nil {
			b = bu.Get([]byte(key))
		}
		return nil
	})
	return b
}

// CreateBucketNested makes sure your database is initialized. Do at beginning for each bucket your program will use.
// Or if creating many buckets dynamically, just make sure CreateBucket is called before any Read operations happen.
func (s *SimpleDB) CreateBucketNested(bucketname string, nestednames ...string) error {
	for _, nestedname := range nestednames {
		if err := s.PutNested(bucketname, nestedname, "", nil); err != nil {
			return err
		}
	}
	return nil
}

// CreateBucket makes sure your database is initialized. Do at beginning for each bucket your program will use.
// Or if creating many buckets dynamically, just make sure CreateBucket is called before any Read operations happen.
// Example: s.CreateBucket("contacts"); s.Unmarshal("contacts", "bob", &user);
func (s *SimpleDB) CreateBucket(bucketnames ...string) error {
	return s.Update(func(tx *Tx) error {
		for _, bucketname := range bucketnames {
			if _, err := tx.CreateBucketIfNotExists([]byte(bucketname)); err != nil {
				return fmt.Errorf("creating bucket %q: %v", bucketname, err)
			}
		}
		return nil
	})
}

var ErrEmptyPath = errors.New("empty path")

func getBucketByPath(tx *bbolt.Tx, path []string, createbuckets bool) (*bbolt.Bucket, error) {
	if len(path) == 0 {
		return nil, fmt.Errorf("bad path: 0")
	}
	var basebucket *Bucket
	if len(path) == 1 && createbuckets {
		if path[0] == "" {
			return nil, ErrEmptyPath
		}
		return tx.CreateBucketIfNotExists([]byte(path[0]))
	}
	spath := path[0]
	if spath == "" {
		return nil, ErrEmptyPath
	}
	basebucket = tx.Bucket([]byte(spath))
	if basebucket == nil {
		return nil, ErrBucketNotFound
	}
	if len(path) == 1 {
		return basebucket, nil
	}
	l := len(path)
	depth := l - 1
	var gotbucket *Bucket
	var err error
	for depth >= 0 {
		spath = path[l-depth]
		basebucket = basebucket.Bucket([]byte(spath))
		if basebucket == nil && !createbuckets {
			return nil, fmt.Errorf("")
		}
		depth--
	}
	return gotbucket, err
}

func (s *SimpleDB) CreatePaths(paths [][]string) error {
	return s.DB().Update(func(tx *bbolt.Tx) error {
		for _, path := range paths {
			l := len(path)
			if l == 0 {
				// panic
				continue
			}
			_, err := getBucketByPath(tx, path, true)
			if err != nil {
				return err
			}
		}
		return nil
	})
}

// MarshalNested writes to database
func (s *SimpleDB) MarshalNested(bucket, nestedname, key string, thing interface{}) error {
	b, err := json.Marshal(thing)
	if err != nil {
		return err
	}
	return s.PutNested(bucket, nestedname, key, b)
}

// Marshal writes to database
func (s *SimpleDB) Marshal(bucket, key string, thing interface{}) error {
	b, err := json.Marshal(thing)
	if err != nil {
		return err
	}
	return s.Put(bucket, key, b)
}

// UpdateNested writes to database
func (s *SimpleDB) PutNested(bucket, nestedname, key string, data []byte) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.Update(func(t *Tx) error {
		b, err := t.CreateBucketIfNotExists([]byte(bucket))
		if err != nil {
			return err
		}
		if nestedname == "" && key == "" && data == nil {
			// just create bucket
			return nil
		}
		n, err := b.CreateBucketIfNotExists([]byte(nestedname))
		if err != nil {
			return err
		}
		return n.Put([]byte(key), data)
	})
}

// Update writes to database
func (s *SimpleDB) Put(bucket, key string, data []byte) error {
	db := s.DB()
	if db == nil {
		return nil
	}
	return db.Update(func(t *Tx) error {
		b, err := t.CreateBucketIfNotExists([]byte(bucket))
		if err != nil {
			return err
		}
		if key == "" && data == nil {
			// just create bucket
			return nil
		}
		return b.Put([]byte(key), data)
	})
}

// KeysNested lists key names within nested bucket
func (s *SimpleDB) KeysNested(bucket, nestedname string) []string {
	db := s.DB()
	if db == nil {
		return nil
	}
	var str []string
	db.View(func(t *Tx) error {
		b := t.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}
		n := b.Bucket([]byte(nestedname))
		if n == nil {
			return nil
		}
		n.ForEach(func(k, v []byte) error {
			str = append(str, string(k))
			return nil
		})
		return nil
	})
	return str
}

// Keys lists key names within named bucket
func (s *SimpleDB) Keys(bucket string) []string {
	db := s.DB()
	if db == nil {
		return nil
	}
	var str []string
	_ = db.View(func(t *Tx) error {
		b := t.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}
		b.ForEach(func(k, v []byte) error {
			str = append(str, string(k))
			return nil
		})
		return nil
	})
	return str
}

// Close the database safely, allowing other processes to use it
func (s *SimpleDB) Close() error {
	return s.DB().Close()
}

// ForEach key in a named bucket...
func (s *SimpleDB) ForEach(bucket string, fn func(k, v []byte) error) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.View(func(t *Tx) error {
		bu := t.Bucket([]byte(bucket))
		return bu.ForEach(fn)
	})
}

// ForEachNested key in a named nested bucket...
func (s *SimpleDB) ForEachNested(bucket, nested string, fn func(k, v []byte) error) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.View(func(t *Tx) error {
		bu := t.Bucket([]byte(bucket))
		if bu == nil {
			return ErrBucketNotFound
		}
		n := bu.Bucket([]byte(nested))
		if n == nil {
			return ErrBucketNotFound
		}

		return n.ForEach(fn)
	})
}

// if a bucket or nested bucket can not be found during Read/View operations
var ErrBucketNotFound = errors.New("bucket not found")

// ForEach bucket...
func (s *SimpleDB) ForEachBucket(fn func(b []byte, bucket *Bucket) error) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.View(func(t *Tx) error {
		return t.ForEach(fn)
	})
}

// Delete keys from bucket
func (s *SimpleDB) Delete(fromBucket string, key ...string) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.Update(func(t *Tx) error {
		b := t.Bucket([]byte(fromBucket))
		if b == nil {
			return ErrBucketNotFound
		}
		for i := range key {
			if err := b.Delete([]byte(key[i])); err != nil {
				return err
			}
		}
		return nil
	})
}

// DeleteNested keys from nested bucket
func (s *SimpleDB) DeleteNested(fromBucket, fromNested string, key ...string) error {
	db := s.DB()
	if db == nil {
		return ErrDBType
	}
	return db.Update(func(t *Tx) error {
		b1 := t.Bucket([]byte(fromBucket))
		if b1 == nil {
			return ErrBucketNotFound
		}
		b := b1.Bucket([]byte(fromNested))
		if b == nil {
			return ErrBucketNotFound
		}
		for i := range key {
			if err := b.Delete([]byte(key[i])); err != nil {
				return err
			}
		}
		return nil
	})
}
