// simpledb package wraps a *bbolt.DB and provides methods for common operations
package simpledb

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"go.etcd.io/bbolt"
)

// SimpleDB ...
type SimpleDB struct {
	//db *bbolt.DB
	D BoltLike
}

// Cursor from bbolt.Cursor
type Cursor = bbolt.Cursor

// DB from bbolt.DB
type DB = bbolt.DB

// Bucket from bbolt.Bucket
type Bucket = bbolt.Bucket

// Tx from bbolt.Tx
type Tx = bbolt.Tx

// Options from bbolt.Options
type Options = bbolt.Options

// BoltLike is any bolt-like DB package.
// This currently only supports bbolt
type BoltLike interface {
	Sync() error
}

// BBolt interface is basically only a *bbolt.DB. Not implemented yet.
type BBolt interface {
	View(func(t *Tx) error) error
	Update(func(t *Tx) error) error
}

// ErrDBType will be given if a programmer is not using the library correctly ;)
var ErrDBType = errors.New("db not found, or type assertion failed, are you not using a *bbolt.DB ?")

// ErrNotFound will be returned by Unmarshal functions if the key did not exist or gave empty value.
// For example, if the requested user does not exist in the database.
var ErrNotFound = fmt.Errorf("not found")

// ErrTimeout returned if we can't get the lock on the db
var ErrTimeout = bbolt.ErrTimeout

// New is an alias for Load()
var New = Load

// Load a database, creating it, and making directories to it, if does not exist.
// Write operations (Update, Marshal, and their nested counterparts) create buckets if they don't exist.
// Read operations (Get, Unmarshal) will return nil if the bucket doesn't exist yet.
func Load(path string, options ...*Options) (*SimpleDB, error) {
	// expand tilde to env
	if strings.HasPrefix(path, "~") {
		path = strings.Replace(path, "~", "$HOME", 1)
	}
	// expand env
	path = os.ExpandEnv(path)

	_, err := os.Stat(path)
	if err != nil && strings.Contains(path, string(filepath.Separator)) {
		dir := filepath.Dir(path)
		if err := os.MkdirAll(dir, 0755); err != nil {
			return nil, err
		}
	}
	var opt *Options
	switch len(options) {
	case 1:
		opt = options[0]
	case 0:
		opt = &Options{Timeout: time.Second * 3}
	default:
		return nil, fmt.Errorf("too many options")
	}
	db, err := bbolt.Open(path, 0600, opt)
	if err != nil {
		return nil, err
	}
	return WrapBoltDB(db), nil
}

// WrapBoltDB ... do not use with non-bbolt db yet
func WrapBoltDB(db BoltLike) *SimpleDB {
	return &SimpleDB{
		D: db,
	}
}
