## simpledb v0.0.5

good wrapper for bbolt database

see [godoc](https://godoc.org/gitlab.com/aerth/simpledb)

or [go.dev](https://pkg.go.dev/gitlab.com/aerth/simpledb)

safe for concurrent use. (?)

## recent api change

api was stable, recently changed Update method

to fix your code, change `Update` to `Put`, or use the older `go get gitlab.com/aerth/simpledb@v0.0.4` in your repo.

the new method `Update` is now the same as `bbolt.Update`, same as `View`.

