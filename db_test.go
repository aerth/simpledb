package simpledb

import (
	"log"
	"os"
	"testing"
)

const testDBpath = "testdata/test.db"

type object struct {
	Name  string
	Value string
}

func getTestDB(t *testing.T) *SimpleDB {
	db, err := Load(testDBpath)
	if err != nil {
		t.Errorf("could not load test database %q: %v", testDBpath, err)
		t.FailNow()
		return nil
	}
	return db
}

var (
	test1 = object{"article1", "here is my article"}
)

func testMarshal(db *SimpleDB, t *testing.T) {
	if err := db.Marshal("test", "test", test1); err != nil {
		t.Error(err)
		return
	}
	got := string(db.Get("test", "test"))
	expected := `{"Name":"article1","Value":"here is my article"}`
	if got != expected {
		t.Errorf("got %q != expected %q", got, expected)
		return
	}
}
func testDelete(db *SimpleDB, t *testing.T) {
	if b := db.Get("testShouldBeNil", "test"); len(b) != 0 {
		log.Fatalln("should be empty but isnt")
	}
	if b := db.Get("test", "test"); len(b) == 0 {
		log.Fatalln("no key to delete")
	}
	if err := db.Delete("test", "test"); err != nil {
		log.Fatalln(err)
	}
	if b := db.Get("test", "test"); len(b) != 0 {
		log.Fatalln("key was not deleted")
	}
	tests := []string{"1", "2", "3", "4"}
	for _, testKey := range tests {
		if err := db.Put("test", testKey, []byte("something")); err != nil {
			log.Fatalln(err)
		}
	}
	if err := db.Delete("test", tests...); err != nil {
		log.Fatalln(err)
	}

}
func testUnmarshal(db *SimpleDB, t *testing.T) {
	// unmarshal
	var test2 object
	if err := db.Unmarshal("test", "test", &test2); err != nil {
		t.Error(err)
		return
	}
	if test1.Name != test2.Name {
		t.Errorf("%q != %q", test1.Name, test2.Name)
		return
	}
	if test1.Value != test2.Value {
		t.Errorf("%q != %q", test1.Value, test2.Value)
		return
	}
}
func tearDown(t *testing.T) {
	os.Remove(testDBpath)
}

func testPath(db *SimpleDB, t *testing.T) {
	// db := getTestDB(t)
	type Foo struct {
		Foo int
		Bar int
	}
	var thing Foo
	var thing2 Foo
	thing.Bar = 42
	thing.Foo = 123
	db.MarshalPath([]string{"base", "messages", "user", "1234", "new"}, thing)
	db.UnmarshalPath([]string{"base", "messages", "user", "1234", "new"}, &thing2)
	if thing.Bar != thing2.Bar {
		t.Fatalf("unequal bar: %d != %d", thing.Bar, thing2.Bar)
	}
	if thing.Foo != thing2.Foo {
		t.Fatalf("unequal Foo: %d != %d", thing.Foo, thing2.Foo)
	}
	var thingbytes []byte
	if err := db.DB().View(func(tx *Tx) error {
		tmp := tx.Bucket([]byte("base")).Bucket([]byte("messages")).Bucket([]byte("user")).Bucket([]byte("1234")).Get([]byte("new"))
		thingbytes = make([]byte, len(tmp))
		copy(thingbytes, tmp)
		return nil
	}); err != nil {
		t.Fatalf("fatla: %v", err)
	}
	// log.Println("bytes:", string(thingbytes))
	if string(thingbytes) != `{"Foo":123,"Bar":42}` {
		t.Fatalf("expected json object, got %q", string(thingbytes))
	}
}

func TestDB(t *testing.T) {
	db := getTestDB(t)
	testMarshal(db, t)
	testUnmarshal(db, t)
	testDelete(db, t)
	testPath(db, t)
	tearDown(t)
}
