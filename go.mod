module gitlab.com/aerth/simpledb

go 1.15

require (
	go.etcd.io/bbolt v1.3.6
	golang.org/x/sys v0.1.0 // indirect
)
